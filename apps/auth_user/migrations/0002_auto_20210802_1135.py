# Generated by Django 3.2.6 on 2021-08-02 11:35

from django.db import migrations


def forwards_func(apps, schema_editor):
    from django.contrib.auth import get_user_model

    user_model = get_user_model()
    obj = user_model(
        username='admin@admin.com',
        email='admin@admin.com',
        is_superuser=True,
        is_staff=True,
        is_active=True
    )
    obj.set_password('q123q123')
    obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('auth_user', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, lambda x: x)
    ]
