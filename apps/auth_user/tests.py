import pytest

from django.urls import reverse


@pytest.mark.django_db
class TestLogin:
    url = reverse('auth_user:login')

    def test_index(self, client):
        response = client.get(self.url)
        assert response.status_code == 200


@pytest.mark.django_db
class TestLogout:
    url = reverse('auth_user:logout')

    def test_index(self, client):
        client.login(username='admin@admin.com', password='q123q123')
        response = client.get(self.url)
        assert response.status_code == 302
        assert response.url == reverse('auth_user:login')
