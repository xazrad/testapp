from django.apps import AppConfig


class AuthUserConfig(AppConfig):
    name = 'auth_user'
    verbose_name = 'Auth User'
