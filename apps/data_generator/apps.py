from django.apps import AppConfig


class DataGeneratorConfig(AppConfig):
    name = 'data_generator'
    verbose_name = 'Data Generator'
