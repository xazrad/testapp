from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class BaseTemplateView(LoginRequiredMixin, TemplateView):
    pass


class DashboardView(BaseTemplateView):
    template_name = "techdata/dashboard.html"
