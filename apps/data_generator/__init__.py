from .celery import app as celery_app


default_app_config = 'data_generator.apps.DataGeneratorConfig'

__all__ = ['celery_app']
