from django.urls import path

from .views import DashboardView


app_name = 'data_generator'

urlpatterns = [
    path('', DashboardView.as_view(), name='dashboard'),
]
