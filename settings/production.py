import dj_database_url

from .base import *  # NoQA
from .additional import *  # NoQA


SECRET_KEY = 'doy83#6&x4zeun%x#7ca!5c+ow00_0xizx!&%v334@x*$*(__9'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': dj_database_url.config()
}

STATIC_ROOT = '/app_static/'

MEDIA_ROOT = '/app_media/'

try:
    from .local import *  # NoQA
except ImportError:
    pass
