from .base import * # NoQA
from .additional import * # NoQA

DEBUG = True


# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = 'http://localhost'

USE_HTTPS = False

DOMAIN = 'localhost:8000'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'testapp',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'app_static')  # NoQA

MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'app_media')  # NoQA

CELERY_BROKER_URL = 'amqp://testapp:testapp@127.0.0.1:5672/testapp'

CELERY_RESULT_BACKEND = 'redis://localhost'


try:
    from .local import *  # NoQA
except ImportError:
    pass
