import os

import dj_database_url

from .base import *  # NoQA
from .additional import *  # NoQA

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True


# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = 'http://localhost'

USE_HTTPS = False

DOMAIN = 'localhost:8000'

DATABASES = {
    'default': dj_database_url.config()
}

STATIC_ROOT = os.path.join(BASE_DIR, 'app_static')  # NoQA

MEDIA_ROOT = os.path.join(BASE_DIR, 'app_media')  # NoQA

try:
    from .local import *  # NoQA
except ImportError:
    pass
