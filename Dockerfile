FROM python:3.8.11-alpine3.14

# Set environment varibles
ENV PYTHONUNBUFFERED=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=off \
    DJANGO_SETTINGS_MODULE=settings.production \
    DATABASE_URL=none

# uWSGI configuration (customize as needed):
ENV UWSGI_WSGI_FILE=/app/wsgi.py \
    UWSGI_SOCKET=:8000 \
    UWSGI_MASTER=1 \
    UWSGI_WORKERS=4 \
    UWSGI_THREADS=8 \
    UWSGI_LAZY_APPS=1 \
    UWSGI_WSGI_ENV_BEHAVIOR=holy

WORKDIR /app/

COPY poetry.lock pyproject.toml /app/

RUN apk add --no-cache --virtual .build-deps \
    gcc postgresql-dev g++ \
    linux-headers musl-dev \
    libffi-dev jpeg-dev zlib-dev \
    tzdata gettext curl \
    && ln -s /usr/include/locale.h /usr/include/xlocale.h \
    && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python \
    && ~/.poetry/bin/poetry config virtualenvs.create false \
    && ~/.poetry/bin/poetry install --no-dev --no-interaction 

COPY . /app/

VOLUME ["/app_static/", "/app_media/"]

EXPOSE 8000
