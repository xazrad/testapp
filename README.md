# TestApp Project

[![coverage report](https://gitlab.com/xazrad/testapp/badges/master/coverage.svg)](https://gitlab.com/xazrad/testapp/commits/master)

## Table of Contents

- [Credentials & links](#credentials-&-links)
- [Run tests](#run-tests)
- [Stage CI/CD](#stage-cicd)
- [Run project local](#run-project-local)

## Credentials & links

**Default superuser**

> user: admin@admin.com
> password: q123q123

**Stage Server**

> https://test.it-open.net

**Django-admin page**

> /admin/

## Run tests

```
pytest --pdb --cov-report term-missing --cov=apps
```

## Stage CI/CD

Register runner

[Install Gitlab Runner](https://docs.gitlab.com/runner/install/)

Permissions

```sh
chown -R gitlab-runner:gitlab-runner /opt/test-app
sudo usermod -aG docker gitlab-runner
```

Register runner

```
gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "TOKEN" \
    --executor "shell" \
    --description "Test APP"
```

Every push to master bransh runs piplene's job.

For skipping tests commit message should contains `:wot:` string.

Example:

```
style(auth_user): PEP8 :wot:
```

For runnning only tests commit message should contain `:ot:` string.

Example:

```
style(auth_user): PEP8 :ot:
```

## Run project local

```sh
python manage.py runserver
```

```
docker-compose up -d --build
```
